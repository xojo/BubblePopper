#tag Class
Protected Class App
Inherits IOSApplication
	#tag CompatibilityFlags = TargetIOS
	#tag Event
		Sub Open()
		  BubbleColors.Append(Color.Red)
		  BubbleColors.Append(Color.Blue)
		  BubbleColors.Append(Color.Green)
		  
		End Sub
	#tag EndEvent


	#tag Property, Flags = &h0
		BubbleColors() As Color
	#tag EndProperty


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
