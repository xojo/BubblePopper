#tag Class
Protected Class Bubble
	#tag Method, Flags = &h0
		Sub Constructor(w As Integer, h As Integer)
		  // Create a bubble at random location and random size
		  Diameter = Math.RandomInt(10, 100)
		  
		  X = Math.RandomInt(0, w - diameter)
		  Y = Math.RandomInt(0, h - diameter)
		  
		  // Pick a color from array of colors
		  FillColor = App.BubbleColors(Math.RandomInt(0, App.BubbleColors.Ubound))
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Draw(g As iOSGraphics)
		  // Draw the bubble at its location, size and color
		  g.FillColor = FillColor
		  g.FillOval(X, Y, Diameter, Diameter)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function WasTapped(tapX As Integer, tapY As Integer) As Boolean
		  If tapX >= X And tapY >= Y And _
		    tapX <= (X + Diameter) And _
		    tapY <= (Y + Diameter) Then
		    Return True
		  Else
		    Return False
		  End If
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private Diameter As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		FillColor As Color
	#tag EndProperty

	#tag Property, Flags = &h21
		Private X As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Y As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="FillColor"
			Group="Behavior"
			InitialValue="&c000000"
			Type="Color"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
