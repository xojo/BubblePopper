#tag IOSView
Begin iosView View1
   BackButtonTitle =   ""
   Compatibility   =   ""
   LargeTitleMode  =   "2"
   Left            =   0
   NavigationBarVisible=   False
   TabIcon         =   ""
   TabTitle        =   ""
   Title           =   ""
   Top             =   0
   Begin iOSCanvas BubbleCanvas
      AccessibilityHint=   ""
      AccessibilityLabel=   ""
      AutoLayout      =   BubbleCanvas, 4, BottomLayoutGuide, 3, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   BubbleCanvas, 1, <Parent>, 1, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   BubbleCanvas, 2, <Parent>, 2, False, +1.00, 4, 1, 0, , True
      AutoLayout      =   BubbleCanvas, 3, TopLayoutGuide, 4, False, +1.00, 4, 1, 0, , True
      Height          =   460.0
      Left            =   0
      LockedInPosition=   False
      Scope           =   2
      Top             =   20
      Visible         =   True
      Width           =   320.0
   End
End
#tag EndIOSView

#tag WindowCode
	#tag Event
		Sub Open()
		  AddBubbles(30)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub AddBubbles(bubbleCount As Integer)
		  For b As Integer = 1 To bubbleCount
		    Bubbles.Append(New Bubble(Self.ContentSize.Width, Self.ContentSize.Height))
		  Next
		  
		  BubbleCanvas.Invalidate
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private Bubbles() As Bubble
	#tag EndProperty


#tag EndWindowCode

#tag Events BubbleCanvas
	#tag Event
		Sub Paint(g As iOSGraphics)
		  For i As Integer = 0 To Bubbles.Ubound
		    Bubbles(i).Draw(g)
		  Next
		End Sub
	#tag EndEvent
	#tag Event
		Sub PointerUp(pos As Xojo.Core.Point, eventInfo As iOSEventInfo)
		  // Check if the tap was within a bubble
		  For b As Integer = Bubbles.Ubound DownTo 0
		    If Bubbles(b).WasTapped(pos.X, pos.Y) Then
		      // Remove bubble because it was tapped
		      Pop.Play
		      Bubbles.Remove(b)
		      BubbleCanvas.Invalidate
		      
		      // If the last bubble was tapped, add more
		      If Bubbles.Ubound = -1 Then
		        AddBubbles(30)
		      End If
		      Return
		    End If
		  Next
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackButtonTitle"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="NavigationBarVisible"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIcon"
		Group="Behavior"
		Type="iOSImage"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabTitle"
		Group="Behavior"
		Type="Text"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Group="Behavior"
		Type="Text"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
#tag EndViewBehavior
